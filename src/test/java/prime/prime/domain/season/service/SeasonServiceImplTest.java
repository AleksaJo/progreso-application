package prime.prime.domain.season.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.quartz.Scheduler;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import prime.prime.domain.account.entity.Account;
import prime.prime.domain.account.entity.AccountStatus;
import prime.prime.domain.account.models.AccountReturnDto;
import prime.prime.domain.role.Role;
import prime.prime.domain.season.entity.Season;
import prime.prime.domain.season.entity.SeasonDurationType;
import prime.prime.domain.season.mapper.SeasonMapper;
import prime.prime.domain.season.models.SeasonCreateDto;
import prime.prime.domain.season.models.SeasonResponseDto;
import prime.prime.domain.season.models.SeasonUpdateDto;
import prime.prime.domain.season.repository.SeasonRepository;
import prime.prime.domain.technology.entity.Technology;
import prime.prime.domain.technology.mapper.TechnologyNameToTechnologyEntity;
import prime.prime.domain.technology.models.TechnologyReturnDto;
import prime.prime.domain.user.entity.User;
import prime.prime.domain.user.models.UserReturnDto;
import prime.prime.domain.user.service.UserService;
import prime.prime.infrastructure.exception.InvalidDateException;
import prime.prime.infrastructure.exception.NotFoundException;
import prime.prime.infrastructure.exception.SeasonException;
import prime.prime.infrastructure.jobs.SeasonReminderJob;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SeasonServiceImplTest {

    @Mock
    private SeasonRepository seasonRepository;

    @Mock
    private SeasonMapper seasonMapper;

    @Mock
    private TechnologyNameToTechnologyEntity technologyNameToTechnology;

    @Mock
    private UserService userService;
    @Mock
    private Scheduler scheduler;
    @Mock
    private SeasonReminderJob seasonReminderJob;
    @Mock
    private SeasonNotificationService seasonNotificationService;

    @InjectMocks
    private SeasonServiceImpl seasonService;
    private static SeasonUpdateDto updateDto;
    private static SeasonResponseDto responseDto;
    private static Season season;
    private static User intern;
    private static User internWithMoreSeasons;
    private static User mentor;
    private static User admin;
    private static UserReturnDto internReturnDto;
    private static UserReturnDto mentorReturnDto;
    private static final Set<User> users = new HashSet<>();


    @BeforeEach
    void setUp() {
        SeasonCreateDto createDto = new SeasonCreateDto("Some season",
            6,
            SeasonDurationType.MONTHS.name(),
            LocalDate.now().plusDays(2),
            LocalDate.now().plusMonths(2),
            Set.of("Java", "Flutter"));

        season = new Season();
        season.setId(1L);
        season.setName(createDto.name());
        season.setDurationType(SeasonDurationType.MONTHS);
        season.setDurationValue(createDto.durationValue());
        season.setStartDate(createDto.startDate());
        season.setEndDate(createDto.endDate());
        season.setTechnologies(Set.of(new Technology(1L, "Java"), new Technology(2L, "Flutter")));

        Account internAccount = new Account();
        internAccount.setRole(Role.INTERN);
        internAccount.setEmail("intern@test.com");
        internAccount.setId(2L);
        internAccount.setStatus(AccountStatus.ACTIVE);

        Account internAccount2 = new Account();
        internAccount2.setId(3L);
        internAccount2.setRole(Role.INTERN);
        internAccount2.setStatus(AccountStatus.ACTIVE);

        AccountReturnDto internAccountReturnDto = new AccountReturnDto(internAccount.getId(),
            internAccount.getEmail(),
            internAccount.getRole(),
            internAccount.getStatus());

        intern = new User();
        intern.setAccount(internAccount);
        intern.setId(2L);
        intern.setLocation("Sofia");
        intern.setFullName("Ivan Ivanov");

        internWithMoreSeasons = new User();
        internWithMoreSeasons.setAccount(internAccount2);
        internWithMoreSeasons.setId(3L);
        internWithMoreSeasons.setLocation("Sofia");
        internWithMoreSeasons.setFullName("Mike Smith");
        internWithMoreSeasons.setSeasons(Set.of(season, new Season(), new Season(), new Season()));

        Account mentorAccount = new Account();
        mentorAccount.setRole(Role.MENTOR);
        internAccount.setEmail("mentor@test.com");
        internAccount.setId(1L);
        mentorAccount.setStatus(AccountStatus.ACTIVE);

        AccountReturnDto mentorAccountReturnDto = new AccountReturnDto(mentorAccount.getId(),
            mentorAccount.getEmail(),
            mentorAccount.getRole(),
            mentorAccount.getStatus());

        mentor = new User();
        mentor.setAccount(mentorAccount);
        mentor.setId(1L);
        mentor.setLocation("Sofia");
        mentor.setFullName("Ivan Ivanov");

        Account adminAccount = new Account();
        adminAccount.setRole(Role.ADMIN);
        adminAccount.setEmail("admin@test.com");
        adminAccount.setId(3L);

        admin = new User();
        admin.setAccount(adminAccount);
        admin.setId(3L);
        admin.setFullName("Tom Tomas");

        internReturnDto = new UserReturnDto(intern.getId(),
            intern.getFullName(),
            intern.getDateOfBirth(),
            intern.getPhoneNumber(),
            intern.getLocation(),
            Set.of(new TechnologyReturnDto(1L, "Java")),
            intern.getImagePath(),
            internAccountReturnDto,
            null);

        mentorReturnDto = new UserReturnDto(mentor.getId(),
            mentor.getFullName(),
            mentor.getDateOfBirth(),
            mentor.getPhoneNumber(),
            mentor.getLocation(),
            Set.of(new TechnologyReturnDto(1L, "Java")),
            mentor.getImagePath(),
            mentorAccountReturnDto,
            null);

        responseDto = new SeasonResponseDto(season.getId(),
            season.getName(),
            season.getDurationValue(),
            season.getDurationType(),
            season.getStartDate(),
            season.getEndDate(),
            List.of(new TechnologyReturnDto(1L, "Java"), new TechnologyReturnDto(2L, "Flutter")),
            Set.of(internReturnDto),
            Set.of(mentorReturnDto));

        updateDto = new SeasonUpdateDto("New name of season",
            6,
            SeasonDurationType.WEEKS.name(),
            LocalDate.now().plusMonths(3),
            LocalDate.now().plusYears(1),
            Set.of("Javascript"),
            Collections.singleton(mentor.getId()), Collections.singleton(intern.getId()));

        users.add(intern);
        users.add(mentor);
        season.setUsers(users);
    }

    @Test
    void create_ValidCreateDto_Successful() {
        SeasonCreateDto createDto = new SeasonCreateDto("Some season",
            6,
            SeasonDurationType.MONTHS.name(),
            LocalDate.now().plusDays(2),
            LocalDate.now().plusMonths(2),
            Set.of("Java", "Flutter"),
            Set.of(1L),
            Set.of(2L));

        Season noIdSeason = new Season();
        noIdSeason.setName(createDto.name());
        noIdSeason.setDurationType(SeasonDurationType.MONTHS);
        noIdSeason.setDurationValue(createDto.durationValue());
        noIdSeason.setStartDate(createDto.startDate());
        noIdSeason.setEndDate(createDto.endDate());
        noIdSeason.setTechnologies(
            Set.of(new Technology(1L, "Java"), new Technology(2L, "Flutter")));
        noIdSeason.setUsers(new HashSet<>(Set.of(mentor, intern)));

        mentor.setSeasons(new HashSet<>(Set.of(noIdSeason)));
        intern.setSeasons(new HashSet<>(Set.of(noIdSeason)));

        when(seasonMapper.fromCreateDto(createDto))
            .thenReturn(noIdSeason);

        when(userService.getAllById(Set.of(1L)))
            .thenReturn(Set.of(mentor));

        when(userService.getAllById(Set.of(2L)))
            .thenReturn(Set.of(intern));

        when(seasonRepository.save(noIdSeason))
            .thenReturn(any(Season.class));

        when(seasonMapper.toResponseDto(noIdSeason))
            .thenReturn(responseDto);

        SeasonResponseDto result = seasonService.create(createDto);

        assertThat(result).usingRecursiveComparison().isEqualTo(responseDto);
    }

    @Test
    void create_InvalidDate_ThrowsInvalidDateException() {
        SeasonCreateDto createDto = new SeasonCreateDto("as", 1,
            SeasonDurationType.MONTHS.name(), LocalDate.now(), LocalDate.now(), Set.of("Java"));

        when(seasonMapper.fromCreateDto(createDto))
            .thenThrow(InvalidDateException.class);

        assertThrows(InvalidDateException.class, () -> seasonService.create(createDto));
    }

    @Test
    void create_InvalidAssigneeRole_ThrowsSeasonException() {
        SeasonCreateDto createDto = new SeasonCreateDto("as",
            1,
            SeasonDurationType.MONTHS.name(),
            LocalDate.now(),
            LocalDate.now(),
            Set.of("Java"),
            Set.of(2L),
            null);

        when(seasonMapper.fromCreateDto(createDto))
            .thenReturn(season);

        when(userService.getAllById(Set.of(2L)))
            .thenReturn(Set.of(intern));

        assertThrows(SeasonException.class, () -> seasonService.create(createDto));
    }

    @Test
    void create_InvalidAccountStatus_ThrowsSeasonException() {
        SeasonCreateDto createDto = new SeasonCreateDto("as",
            1,
            SeasonDurationType.MONTHS.name(),
            LocalDate.now(),
            LocalDate.now(),
            Set.of("Java"),
            null,
            Set.of(2L));
        intern.getAccount().setStatus(AccountStatus.ARCHIVED);

        when(seasonMapper.fromCreateDto(createDto))
            .thenReturn(season);

        when(userService.getAllById(Set.of(2L)))
            .thenReturn(Set.of(intern));

        assertThrows(SeasonException.class, () -> seasonService.create(createDto));
    }

    @Test
    void create_InternMaxSeasonLimit_ThrowsSeasonException() {
        Account account = new Account();
        account.setId(3L);
        account.setRole(Role.INTERN);
        account.setStatus(AccountStatus.ACTIVE);
        internWithMoreSeasons.setAccount(account);

        SeasonCreateDto createDto = new SeasonCreateDto("as",
            1,
            SeasonDurationType.MONTHS.name(),
            LocalDate.now(),
            LocalDate.now(),
            Set.of("Java"),
            null,
            Set.of(internWithMoreSeasons.getId()));

        when(seasonMapper.fromCreateDto(createDto))
            .thenReturn(season);

        when(userService.getAllById(Set.of(3L)))
            .thenReturn(Set.of(internWithMoreSeasons));

        assertThrows(SeasonException.class, () -> seasonService.create(createDto));
    }

    @Test
    void getById_ValidId_Success() {
        when(seasonRepository.findByIdAndFetchUsers(season.getId()))
            .thenReturn(Optional.of(season));

        when(seasonMapper.toResponseDto(season))
            .thenReturn(responseDto);

        SeasonResponseDto result = seasonService.getById(season.getId());

        assertThat(result).usingRecursiveComparison().isEqualTo(responseDto);
    }

    @Test
    void getById_InvalidId_ThrowsNotFoundException() {
        when(seasonRepository.findByIdAndFetchUsers(season.getId()))
            .thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> seasonService.getById(1L));
    }

    @Test
    void getAll_Valid_Successful() {
        Page<Season> page = new PageImpl<>(List.of(season));

        List<Season> seasons = List.of(season);

        when(seasonRepository.findAllAndFetchUsers(any(Pageable.class)))
            .thenReturn(seasons);

        when(seasonMapper.toResponseDto(season))
            .thenReturn(responseDto);

        Page<SeasonResponseDto> result = seasonService.getAll(Pageable.ofSize(20));

        assertEquals(page.getTotalElements(), result.getTotalElements());
        assertEquals(page.getTotalPages(), result.getTotalPages());
        assertFalse(result.isEmpty());
    }

    @Test
    void getAll_EmptyPage_Successful() {
        List<Season> seasons = new ArrayList<>();
        when(seasonRepository.findAllAndFetchUsers(any(Pageable.class)))
            .thenReturn(seasons);

        Page<SeasonResponseDto> result = seasonService.getAll(Pageable.ofSize(20));

        assertTrue(result.isEmpty());
    }

    @Test
    void update_ValidUpdateDto_Successful() {
        when(seasonRepository.findByIdAndFetchUsers(season.getId()))
            .thenReturn(Optional.of(season));

        when(technologyNameToTechnology.toTechnologyEntity(updateDto.technologies()))
            .thenReturn(Set.of(new Technology(3L, "Javascript")));

        doNothing().when(seasonMapper).updateFromUpdateDto(updateDto, season);

        season.setName(updateDto.name());
        season.setDurationType(SeasonDurationType.WEEKS);
        season.setDurationValue(updateDto.durationValue());
        season.setStartDate(updateDto.startDate());
        season.setEndDate(updateDto.endDate());
        season.setTechnologies(Set.of(new Technology(3L, "Javascript")));
        season.setUsers(new HashSet<>(Set.of(mentor, intern)));

        SeasonResponseDto newResponseDto = new SeasonResponseDto(season.getId(),
            season.getName(),
            season.getDurationValue(),
            season.getDurationType(),
            season.getStartDate(),
            season.getEndDate(),
            List.of(new TechnologyReturnDto(3L, "Javascript")),
            Set.of(mentorReturnDto),
            Set.of(internReturnDto));

        when(seasonRepository.save(season))
            .thenReturn(season);

        when(seasonMapper.toResponseDto(season))
            .thenReturn(newResponseDto);

        SeasonResponseDto result = seasonService.update(season.getId(), updateDto);

        assertThat(result).usingRecursiveComparison().isEqualTo(newResponseDto);
    }

    @Test
    void update_InvalidId_ThrowsNotFoundException() {
        when(seasonRepository.findByIdAndFetchUsers(season.getId()))
            .thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> seasonService.update(1L, updateDto));
    }

    @Test
    void update_InvalidDate_ThrowsInvalidDateException() {
        when(seasonRepository.findByIdAndFetchUsers(season.getId()))
            .thenReturn(Optional.of(season));

        SeasonUpdateDto seasonUpdateDto = new SeasonUpdateDto("New name of season",
            6,
            SeasonDurationType.WEEKS.name(),
            LocalDate.now().plusMonths(3),
            LocalDate.now().plusMonths(2),
            Set.of("Javascript"), null, null);

        doThrow(InvalidDateException.class).when(seasonMapper)
            .updateFromUpdateDto(seasonUpdateDto, season);

        assertThrows(InvalidDateException.class, () -> seasonService.update(1L, seasonUpdateDto));
    }

    @Test
    void update_InvalidAssigneeRole_ThrowsSeasonException() {
        when(seasonRepository.findByIdAndFetchUsers(season.getId()))
            .thenReturn(Optional.of(season));

        SeasonUpdateDto invalidUpdateDto = new SeasonUpdateDto("as",
            1,
            SeasonDurationType.MONTHS.name(),
            LocalDate.now(),
            LocalDate.now(),
            Set.of("Java"),
            Set.of(2L),
            null);

        when(userService.getAllById(Set.of(2L)))
            .thenReturn(Set.of(intern));

        assertThrows(SeasonException.class, () -> seasonService.update(1L, invalidUpdateDto));
    }

    @Test
    void update_InvalidAccountStatus_ThrowsSeasonException() {
        when(seasonRepository.findByIdAndFetchUsers(season.getId()))
            .thenReturn(Optional.of(season));

        SeasonUpdateDto invalidUpdateDto = new SeasonUpdateDto("as",
            1,
            SeasonDurationType.MONTHS.name(),
            LocalDate.now(),
            LocalDate.now(),
            Set.of("Java"),
            null,
            Set.of(2L));
        intern.getAccount().setStatus(AccountStatus.ARCHIVED);

        when(userService.getAllById(Set.of(2L)))
            .thenReturn(Set.of(intern));

        assertThrows(SeasonException.class, () -> seasonService.update(1L, invalidUpdateDto));
    }

    @Test
    void update_InternMaxSeasonLimit_ThrowsSeasonException() {
        when(seasonRepository.findByIdAndFetchUsers(season.getId()))
            .thenReturn(Optional.of(season));

        SeasonUpdateDto seasonUpdateDto = new SeasonUpdateDto("as",
            1,
            SeasonDurationType.MONTHS.name(),
            LocalDate.now(),
            LocalDate.now(),
            Set.of("Java"),
            Set.of(1L),
            Set.of(3L));
        Set<Long> ids = new HashSet<>();
        ids.add(1L);

        when(userService.getAllById(ids))
            .thenReturn(Set.of(internWithMoreSeasons));

        assertThrows(SeasonException.class, () -> seasonService.update(1L, seasonUpdateDto));
    }

    @Test
    void delete_ValidId_Successful() {
        when(seasonRepository.findByIdAndFetchUsers(season.getId())).thenReturn(
            Optional.of(season));

        seasonService.delete(season.getId());

        verify(seasonRepository, times(1)).deleteById(1L);
    }

    @Test
    void delete_InvalidId_ThrowsNotFoundException() {
        when(seasonRepository.findByIdAndFetchUsers(season.getId()))
            .thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> seasonService.delete(1L));
    }

    @Test
    void findById_ValidId_Successful() {
        when(seasonRepository.findById(season.getId())).thenReturn(Optional.of(season));

        Season returnedSeason = seasonService.findById(season.getId());

        assertEquals("Some season", returnedSeason.getName());
        assertEquals(6, returnedSeason.getDurationValue());
        assertEquals(SeasonDurationType.MONTHS, returnedSeason.getDurationType());
    }

    @Test
    void findById_InvalidId_ThrowsNotFoundException() {
        assertThrows(NotFoundException.class, () -> seasonService.findById(10L));
    }

    @Test
    void findActiveSeason_ExistingSeasonsIntern_Successful() {
        Season newSeason = new Season();
        newSeason.setId(2L);
        newSeason.setDurationType(SeasonDurationType.MONTHS);
        newSeason.setDurationValue(6);
        newSeason.setStartDate(LocalDate.now().minusDays(1));
        newSeason.setEndDate(LocalDate.now().plusMonths(6));
        intern.setSeasons(Set.of(newSeason));

        Season returnedSeason = seasonService.findActiveSeason(intern, 2L);

        assertEquals(2L, returnedSeason.getId());
    }

    @Test
    void findActiveSeason_NotActiveSeasonIntern_ThrowsSeasonException() {
        intern.setSeasons(Set.of(season));
        assertThrows(SeasonException.class, () -> seasonService.findActiveSeason(intern, 1L));
    }

    @Test
    void findActiveSeason_NonExistingSeasonsIntern_ThrowsSeasonException() {
        intern.setSeasons(null);
        assertThrows(SeasonException.class, () -> seasonService.findActiveSeason(intern, 1L));
    }

    @Test
    void findActiveSeason_ActiveSeasonAdmin_Successful() {
        Season newSeason = new Season();
        newSeason.setId(2L);
        newSeason.setDurationType(SeasonDurationType.MONTHS);
        newSeason.setDurationValue(6);
        newSeason.setStartDate(LocalDate.now().minusDays(1));
        newSeason.setEndDate(LocalDate.now().plusMonths(6));
        when(seasonRepository.findById(newSeason.getId())).thenReturn(Optional.of(newSeason));

        Season returnedSeason = seasonService.findActiveSeason(admin, 2L);

        assertEquals(2L, returnedSeason.getId());
    }
}