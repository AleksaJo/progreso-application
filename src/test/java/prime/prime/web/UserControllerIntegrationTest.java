package prime.prime.web;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static prime.prime.web.TestUtils.toJson;

import com.icegreen.greenmail.configuration.GreenMailConfiguration;
import com.icegreen.greenmail.junit5.GreenMailExtension;
import com.icegreen.greenmail.util.ServerSetupTest;
import java.time.LocalDate;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import prime.prime.domain.account.entity.Account;
import prime.prime.domain.account.entity.AccountStatus;
import prime.prime.domain.account.models.AccountCreateDto;
import prime.prime.domain.account.models.AccountUpdateDto;
import prime.prime.domain.account.models.ChangePasswordDto;
import prime.prime.domain.role.Role;
import prime.prime.domain.technology.entity.Technology;
import prime.prime.domain.user.entity.User;
import prime.prime.domain.user.models.UserCreateDto;
import prime.prime.domain.user.models.UserUpdateDto;
import prime.prime.domain.user.repository.UserRepository;
import prime.prime.infrastructure.exception.ArchivedUserException;
import prime.prime.infrastructure.exception.ChangePasswordAccessException;
import prime.prime.infrastructure.exception.DuplicateException;
import prime.prime.infrastructure.exception.InvalidOldPasswordException;
import prime.prime.infrastructure.exception.NotFoundException;
import prime.prime.infrastructure.security.JwtUtil;
import prime.prime.infrastructure.security.ProgresoUserDetails;

@SpringBootTest
@Testcontainers
@AutoConfigureMockMvc
@Transactional
@TestPropertySource("classpath:application-test.properties")
class UserControllerIntegrationTest {

    private static final String USER_ENDPOINT = "/users";

    private static final String ROLE_ADMIN = "ROLE_ADMIN";

    private static final String ROLE_MENTOR = "ROLE_MENTOR";

    private static final String ROLE_INTERN = "ROLE_INTERN";

    private static final String USERNAME_ADMIN = "admin@xprmnt.xyz";

    private static final String USERNAME_MENTOR = "mentor@progreso.com";

    private static final String JWT = "abdcd.dd.d";

    @Container
    private static MySQLContainer container = new MySQLContainer("mysql:8.0.32")
        .withDatabaseName("progreso");

    @RegisterExtension
    static GreenMailExtension greenMail = new GreenMailExtension(ServerSetupTest.SMTP)
        .withConfiguration(GreenMailConfiguration.aConfig().withUser("progreso", "password"))
        .withPerMethodLifecycle(false);


    private static UserCreateDto userCreateDto;

    private static AccountCreateDto accountCreateDto;

    private static UserUpdateDto userUpdateDto;

    private static AccountUpdateDto accountUpdateDto;

    private static User user;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private JwtUtil jwtUtil;

    @Autowired
    private UserRepository userRepository;

    @DynamicPropertySource
    public static void overrideProperties(DynamicPropertyRegistry registry) {
        TestUtils.configureDBContainer(registry, container);
    }

    @BeforeAll
    static void setUp() {
        accountCreateDto = new AccountCreateDto("testing@gmail.com",
            Role.INTERN.name());

        userCreateDto = new UserCreateDto("Ivan Ivanov",
            LocalDate.of(1996, 5, 17),
            "+106380287",
            "Sofia, Bulgaria",
            accountCreateDto,
            Set.of("Java"));

        accountUpdateDto = new AccountUpdateDto(Role.INTERN.name());

        userUpdateDto = new UserUpdateDto("Ivan Draganov",
            LocalDate.of(1996, 5, 17),
            "+106380287",
            "Sofia, Bulgaria",
            accountUpdateDto,
            Set.of("Java"),
            null
        );

        Account account = new Account();
        account.setEmail(userCreateDto.account().email());
        account.setRole(Role.INTERN);
        account.setStatus(AccountStatus.INVITED);
        account.setPassword("$2a$10$mh1A.eqzfdUVKWndBqPrreN6QUEdLFcVqd85/.knzzhiuzPKCe4me");

        user = new User();
        user.setFullName(userCreateDto.fullName());
        user.setLocation(userCreateDto.location());
        user.setDateOfBirth(userCreateDto.dateOfBirth());
        user.setPhoneNumber(userCreateDto.phoneNumber());
        user.setAccount(account);
        user.setTechnologies(Set.of(new Technology(1L, "Java")));
    }

    @Test
    @WithMockUser(username = USERNAME_ADMIN, authorities = ROLE_ADMIN)
    void create_ValidUserCreateDTO_Ok() throws Exception {
        user.getAccount().setStatus(AccountStatus.INVITED);

        mockMvc.perform(post(USER_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(userCreateDto)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.fullName").value("Ivan Ivanov"))
            .andExpect(jsonPath("$.dateOfBirth").value("1996-05-17"))
            .andExpect(jsonPath("$.phoneNumber").value("+106380287"))
            .andExpect(jsonPath("$.location").value("Sofia, Bulgaria"))
            .andExpect(jsonPath("$.account.email").value("testing@gmail.com"))
            .andExpect(jsonPath("$.account.role").value("INTERN"))
            .andExpect(jsonPath("$.account.status").value("INVITED"));

        MimeMessage receivedMessage = greenMail.getReceivedMessages()[0];

        assertEquals("Account created", receivedMessage.getSubject());
        assertEquals(1, receivedMessage.getAllRecipients().length);
        assertEquals(user.getAccount().getEmail(),
            receivedMessage.getAllRecipients()[0].toString());
    }

    @Test
    @WithMockUser(username = USERNAME_MENTOR, authorities = ROLE_MENTOR)
    void create_User_ForbiddenForMentor() throws Exception {
        mockMvc.perform(post(USER_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(userCreateDto)))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithAnonymousUser
    void create_User_UnauthorizedForAnonymous() throws Exception {
        mockMvc.perform(post(USER_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(userCreateDto)))
            .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = USERNAME_ADMIN, authorities = ROLE_ADMIN)
    void create_InvalidUserCreateDto_BadRequest() throws Exception {
        UserCreateDto invalidDto = new UserCreateDto("Iv",
            LocalDate.of(2958, 5, 17),
            "abcd",
            "Pl",
            accountCreateDto,
            null);

        mockMvc.perform(post(USER_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(invalidDto)))
            .andExpect(status().isBadRequest())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.violations[?(@.field==\"dateOfBirth\")].error").value(
                "Date of birth is not correct"))
            .andExpect(jsonPath("$.violations[?(@.field==\"fullName\")].error").value(
                "Full name should be between 3 and 100 characters long"))
            .andExpect(jsonPath("$.violations[?(@.field==\"location\")].error").value(
                "Location should be at least 3 characters long"))
            .andExpect(jsonPath("$.violations[?(@.field==\"technologies\")].error").value(
                "Technologies are mandatory"))
            .andExpect(
                jsonPath("$.violations[?(@.field==\"phoneNumber\")].error").value("Invalid phone"));
    }

    @Test
    @WithMockUser(username = USERNAME_ADMIN, authorities = ROLE_ADMIN)
    void create_UserWithEmailAlreadyExists_Conflict() throws Exception {
        userRepository.save(user);

        mockMvc.perform(post(USER_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(userCreateDto)))
            .andExpect(status().isConflict())
            .andExpect(result -> assertTrue(
                result.getResolvedException() instanceof DuplicateException))
            .andExpect(result -> assertEquals(
                "Account with email " + userCreateDto.account().email() + " already exists",
                Objects.requireNonNull(
                    result.getResolvedException()).getMessage()));
    }

    @Test
    @WithMockUser(username = USERNAME_ADMIN, authorities = ROLE_ADMIN)
    void getById_ValidId_Ok() throws Exception {
        mockMvc.perform(get(USER_ENDPOINT + "/1"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.fullName").value("Administrator"))
            .andExpect(jsonPath("$.dateOfBirth").value(LocalDate.now().toString()))
            .andExpect(jsonPath("$.phoneNumber").value("0000000000"))
            .andExpect(jsonPath("$.account.email").value("admin@xprmnt.xyz"))
            .andExpect(jsonPath("$.account.role").value("ADMIN"))
            .andExpect(jsonPath("$.account.status").value("ACTIVE"));
    }

    @Test
    @WithMockUser(username = USERNAME_ADMIN, authorities = ROLE_ADMIN)
    void getById_InvalidId_NotFound() throws Exception {
        mockMvc.perform(get(USER_ENDPOINT + "/5"))
            .andExpect(status().isNotFound())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(result -> assertTrue(
                result.getResolvedException() instanceof NotFoundException))
            .andExpect(
                result -> assertEquals(User.class.getSimpleName() + " with id : 5 not found;",
                    Objects.requireNonNull(
                        result.getResolvedException()).getMessage()));
    }

    @Test
    @WithAnonymousUser
    void getById_ValidId_UnauthorizedForAnonymous() throws Exception {
        mockMvc.perform(get(USER_ENDPOINT + "/1"))
            .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = USERNAME_ADMIN, authorities = ROLE_ADMIN)
    void getAll_Ok() throws Exception {
        mockMvc.perform(get(USER_ENDPOINT))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.totalElements").value(1))
            .andExpect(jsonPath("$.content[?(@.id == 1)].account.email").value("admin@xprmnt.xyz"));
    }

    @Test
    @WithAnonymousUser
    void getAll_User_UnauthorizedForAnonymous() throws Exception {
        mockMvc.perform(get(USER_ENDPOINT))
            .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = USERNAME_ADMIN, authorities = ROLE_ADMIN)
    void delete_ValidId_Ok() throws Exception {
        User saved = userRepository.save(user);

        mockMvc.perform(delete(USER_ENDPOINT + "/" + saved.getId().toString()))
            .andExpect(status().isNoContent());

        assertFalse(userRepository.existsById(saved.getId()));
    }

    @Test
    @WithMockUser(username = USERNAME_ADMIN, authorities = ROLE_ADMIN)
    void delete_InvalidId_NotFound() throws Exception {
        mockMvc.perform(delete(USER_ENDPOINT + "/2"))
            .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = USERNAME_MENTOR, authorities = ROLE_MENTOR)
    void delete_User_ForbiddenForNonAdmin() throws Exception {
        mockMvc.perform(delete(USER_ENDPOINT + "/2"))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithAnonymousUser
    void delete_User_UnauthorizedForAnonymous() throws Exception {
        mockMvc.perform(delete(USER_ENDPOINT + "/2"))
            .andExpect(status().isUnauthorized());
    }

    @Test
    void update_ValidUpdateDTO_Ok() throws Exception {
        user.getAccount().setStatus(AccountStatus.INVITED);
        User saved = userRepository.save(user);

        MockMultipartFile updateDto = new MockMultipartFile(
            "user",
            "",
            MediaType.APPLICATION_JSON_VALUE,
            toJson(userUpdateDto).getBytes()
        );

        UserDetails admin = new ProgresoUserDetails(1L, 1L, USERNAME_ADMIN, "Admin123+",
            Collections.singletonList(new SimpleGrantedAuthority(ROLE_ADMIN)));

        mockMvc.perform(
                multipart(HttpMethod.PATCH, USER_ENDPOINT + "/" + saved.getId().toString()).file(
                        updateDto)
                    .with(user(admin))
            )
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.fullName").value("Ivan Draganov"))
            .andExpect(jsonPath("$.dateOfBirth").value("1996-05-17"))
            .andExpect(jsonPath("$.phoneNumber").value("+106380287"))
            .andExpect(jsonPath("$.location").value("Sofia, Bulgaria"))
            .andExpect(jsonPath("$.account.email").value("testing@gmail.com"))
            .andExpect(jsonPath("$.account.role").value("INTERN"))
            .andExpect(jsonPath("$.account.status").value("INVITED"));
    }

    @Test
    @WithAnonymousUser
    void update_ValidUpdateDto_UnauthorizedForAnonymous() throws Exception {
        User saved = userRepository.save(user);

        MockMultipartFile updateDto = new MockMultipartFile(
            "user",
            "",
            MediaType.APPLICATION_JSON_VALUE,
            toJson(userUpdateDto).getBytes()
        );

        mockMvc.perform(multipart(HttpMethod.PATCH, USER_ENDPOINT + "/" + saved.getId().toString())
                .file(updateDto))
            .andExpect(status().isUnauthorized());
    }

    @Test
    void update_InvalidUpdateDTO_BadRequest() throws Exception {
        User saved = userRepository.save(user);

        UserUpdateDto invalidDto = new UserUpdateDto("Iv",
            LocalDate.of(2958, 5, 17),
            "abcd",
            "Pl",
            accountUpdateDto,
            null,
            null);

        MockMultipartFile updateDto = new MockMultipartFile(
            "user",
            "",
            MediaType.APPLICATION_JSON_VALUE,
            toJson(invalidDto).getBytes()
        );

        UserDetails admin = new ProgresoUserDetails(1L, 1L, USERNAME_ADMIN, "Admin123+",
            Collections.singletonList(new SimpleGrantedAuthority(ROLE_ADMIN)));

        mockMvc.perform(
                multipart(HttpMethod.PATCH, USER_ENDPOINT + "/" + saved.getId().toString()).file(
                        updateDto)
                    .with(user(admin))
            )
            .andExpect(status().isBadRequest())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.violations[?(@.field==\"dateOfBirth\")].error").value(
                "Date of birth is not correct"))
            .andExpect(jsonPath("$.violations[?(@.field==\"fullName\")].error").value(
                "Full name should be between 3 and 100 characters long"))
            .andExpect(jsonPath("$.violations[?(@.field==\"location\")].error").value(
                "Location should be at least 3 characters long"))
            .andExpect(
                jsonPath("$.violations[?(@.field==\"phoneNumber\")].error").value("Invalid phone"));
    }

    @Test
    void update_InvalidId_NotFound() throws Exception {
        MockMultipartFile updateDto = new MockMultipartFile(
            "user",
            "",
            MediaType.APPLICATION_JSON_VALUE,
            toJson(userUpdateDto).getBytes()
        );

        UserDetails admin = new ProgresoUserDetails(1L, 1L, USERNAME_ADMIN, "Admin123+",
            Collections.singletonList(new SimpleGrantedAuthority(ROLE_ADMIN)));

        mockMvc.perform(
                multipart(HttpMethod.PATCH, USER_ENDPOINT + "/2").file(updateDto)
                    .with(user(admin))
            )
            .andExpect(status().isNotFound())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(result -> assertTrue(
                result.getResolvedException() instanceof NotFoundException))
            .andExpect(
                result -> assertEquals(User.class.getSimpleName() + " with id : 2 not found;",
                    Objects.requireNonNull(
                        result.getResolvedException()).getMessage()));
    }

    @Test
    void update_updateRole_ForbiddenForNonAdmin() throws Exception {
        user.getAccount().setStatus(AccountStatus.ACTIVE);
        User saved = userRepository.save(user);

        AccountUpdateDto mentorAccountUpdateDto = new AccountUpdateDto(Role.MENTOR.name());
        UserUpdateDto invalidUpdateDto = new UserUpdateDto(userUpdateDto.fullName(),
            userUpdateDto.dateOfBirth(),
            userUpdateDto.phoneNumber(), userUpdateDto.location(), mentorAccountUpdateDto,
            userUpdateDto.technologies(),
            null);

        MockMultipartFile updateDto = new MockMultipartFile(
            "user",
            "",
            MediaType.APPLICATION_JSON_VALUE,
            toJson(invalidUpdateDto).getBytes()
        );

        UserDetails intern = new ProgresoUserDetails(2L, 2L, saved.getAccount().getEmail(),
            saved.getAccount().getPassword(),
            Collections.singletonList(new SimpleGrantedAuthority(ROLE_INTERN)));

        mockMvc.perform(
                multipart(HttpMethod.PATCH, USER_ENDPOINT + "/" + saved.getId().toString()).file(
                        updateDto)
                    .with(user(intern))
            )
            .andExpect(status().isForbidden());
    }

    @Test
    void changePassword_ValidChangePasswordDTO_Ok() throws Exception {
        user.getAccount().setStatus(AccountStatus.ACTIVE);
        User saved = userRepository.save(user);

        ChangePasswordDto changePasswordDto = new ChangePasswordDto("2J#Hx-Xo",
            "AbcdE456+");

        UserDetails intern = new ProgresoUserDetails(saved.getId(), saved.getId(),
            saved.getAccount().getEmail(), saved.getAccount().getPassword(),
            Collections.singletonList(new SimpleGrantedAuthority(ROLE_INTERN)));

        when(jwtUtil.getJwtFromRequest(any(HttpServletRequest.class))).thenReturn(JWT);
        when(jwtUtil.getUsername(JWT)).thenReturn(saved.getAccount().getEmail());

        mockMvc.perform(patch(USER_ENDPOINT + "/" + saved.getId().toString() + "/password")
                .with(user(intern))
                .header("Authorization", "Bearer " + JWT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(changePasswordDto)))
            .andExpect(status().isOk());
    }

    @Test
    void changePassword_InvalidNewPassword_BadRequest() throws Exception {
        user.getAccount()
            .setPassword("$2a$10$mh1A.eqzfdUVKWndBqPrreN6QUEdLFcVqd85/.knzzhiuzPKCe4me");
        User saved = userRepository.save(user);

        ChangePasswordDto changePasswordDto = new ChangePasswordDto("2J#Hx-Xo",
            "fff");

        UserDetails intern = new ProgresoUserDetails(saved.getId(), saved.getId(),
            saved.getAccount().getEmail(), saved.getAccount().getPassword(),
            Collections.singletonList(new SimpleGrantedAuthority(ROLE_INTERN)));

        when(jwtUtil.getJwtFromRequest(any(HttpServletRequest.class))).thenReturn(JWT);
        when(jwtUtil.getUsername(JWT)).thenReturn(saved.getAccount().getEmail());

        mockMvc.perform(patch(USER_ENDPOINT + "/" + saved.getId().toString() + "/password")
                .with(user(intern))
                .header("Authorization", "Bearer " + JWT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(changePasswordDto)))
            .andExpect(status().isBadRequest());
    }

    @Test
    void changePassword_IncorrectOldPassword_Conflict() throws Exception {
        user.getAccount()
            .setPassword("$2a$10$mh1A.eqzfdUVKWndBqPrreN6QUEdLFcVqd85/.knzzhiuzPKCe4me");
        User saved = userRepository.save(user);

        ChangePasswordDto changePasswordDto = new ChangePasswordDto("2J#Hx-X5564564o",
            "AbcdE456+");

        UserDetails intern = new ProgresoUserDetails(saved.getId(), saved.getId(),
            saved.getAccount().getEmail(), saved.getAccount().getPassword(),
            Collections.singletonList(new SimpleGrantedAuthority(ROLE_INTERN)));

        when(jwtUtil.getJwtFromRequest(any(HttpServletRequest.class))).thenReturn(JWT);
        when(jwtUtil.getUsername(JWT)).thenReturn(saved.getAccount().getEmail());

        mockMvc.perform(patch(USER_ENDPOINT + "/" + saved.getId().toString() + "/password")
                .with(user(intern))
                .header("Authorization", "Bearer " + JWT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(changePasswordDto)))
            .andExpect(status().isConflict())
            .andExpect(result -> assertTrue(
                result.getResolvedException() instanceof InvalidOldPasswordException))
            .andExpect(result -> assertEquals(
                "Incorrect old password",
                Objects.requireNonNull(
                    result.getResolvedException()).getMessage()));
    }

    @Test
    void changePassword_NotCurrentlyLoggedInUser_Conflict() throws Exception {
        user.getAccount()
            .setPassword("$2a$10$mh1A.eqzfdUVKWndBqPrreN6QUEdLFcVqd85/.knzzhiuzPKCe4me");
        User saved = userRepository.save(user);

        ChangePasswordDto changePasswordDto = new ChangePasswordDto("2J#Hx-Xo",
            "AbcdE456+");

        UserDetails intern = new ProgresoUserDetails(saved.getId(), saved.getId(),
            saved.getAccount().getEmail(), saved.getAccount().getPassword(),
            Collections.singletonList(new SimpleGrantedAuthority(ROLE_INTERN)));

        when(jwtUtil.getJwtFromRequest(any(HttpServletRequest.class))).thenReturn(JWT);
        when(jwtUtil.getUsername(JWT)).thenReturn(saved.getAccount().getEmail());

        mockMvc.perform(patch(USER_ENDPOINT + "/50/password")
                .with(user(intern))
                .header("Authorization", "Bearer " + JWT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(changePasswordDto)))
            .andExpect(status().isConflict())
            .andExpect(result -> assertTrue(
                result.getResolvedException() instanceof ChangePasswordAccessException))
            .andExpect(result -> assertEquals(
                "User can change its own password only",
                Objects.requireNonNull(
                    result.getResolvedException()).getMessage()));
    }

    @Test
    @WithAnonymousUser
    void changePassword_User_UnauthorizedForAnonymous() throws Exception {
        ChangePasswordDto changePasswordDto = new ChangePasswordDto("2J#Hx-Xo",
            "AbcdE456+");

        mockMvc.perform(patch(USER_ENDPOINT + "/50/password")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(changePasswordDto)))
            .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = USERNAME_ADMIN, authorities = ROLE_ADMIN)
    void archive_ValidUser_Ok() throws Exception {
        User saved = userRepository.save(user);

        mockMvc.perform(patch(USER_ENDPOINT + "/" + saved.getId().toString() + "/archive"))
            .andExpect(status().isOk());

        User archived = userRepository.findById(saved.getId()).orElseThrow();
        assertTrue(archived.getAccount().getStatus().equals(AccountStatus.ARCHIVED));
    }

    @Test
    @WithMockUser(username = USERNAME_ADMIN, authorities = ROLE_ADMIN)
    void archive_AlreadyArchived_NotFound() throws Exception {
        user.getAccount().setStatus(AccountStatus.ARCHIVED);
        User saved = userRepository.save(user);

        mockMvc.perform(patch(USER_ENDPOINT + "/" + saved.getId().toString() + "/archive"))
            .andExpect(status().isNotFound())
            .andExpect(result -> assertTrue(
                result.getResolvedException() instanceof ArchivedUserException))
            .andExpect(result -> assertEquals(
                Account.class.getSimpleName() + " with id : " + saved.getId().toString()
                    + " is archived",
                Objects.requireNonNull(
                    result.getResolvedException()).getMessage()));
    }

    @Test
    @WithMockUser(username = USERNAME_ADMIN, authorities = ROLE_ADMIN)
    void archive_NotExistingUser_NotFound() throws Exception {
        mockMvc.perform(patch(USER_ENDPOINT + "/2/archive"))
            .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = USERNAME_MENTOR, authorities = ROLE_MENTOR)
    void archive_User_ForbiddenForNonAdmin() throws Exception {
        mockMvc.perform(patch(USER_ENDPOINT + "/2/archive"))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithAnonymousUser
    void archive_User_UnauthorizedForAnonymous() throws Exception {
        mockMvc.perform(patch(USER_ENDPOINT + "/2/archive"))
            .andExpect(status().isUnauthorized());
    }
}