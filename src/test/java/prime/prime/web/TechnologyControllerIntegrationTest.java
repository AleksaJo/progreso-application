package prime.prime.web;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import java.util.Objects;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
import prime.prime.domain.technology.entity.Technology;
import prime.prime.domain.technology.models.TechnologyCreateDto;
import prime.prime.domain.technology.repository.TechnologyRepository;
import prime.prime.infrastructure.exception.DuplicateException;

@Testcontainers
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class TechnologyControllerIntegrationTest {

    private static TechnologyCreateDto technologyCreateDto;
    private static final String URL_PREFIX = "/technologies";
    private static final String ROLE_ADMIN = "ROLE_ADMIN";
    private static final String ROLE_MENTOR = "ROLE_MENTOR";
    private static final String ROLE_INTERN = "ROLE_INTERN";
    private static final String USERNAME_ADMIN = "admin@xprmnt.xyz";
    private static final String USERNAME_MENTOR = "mentor@example.com";
    private static final String USERNAME_INTERN = "intern@example.com";
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private TechnologyRepository technologyRepository;

    @Container
    private final static MySQLContainer container = new MySQLContainer("mysql:8.0.30")
        .withDatabaseName("progreso");

    @DynamicPropertySource
    public static void overrideProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", container::getJdbcUrl);
        registry.add("spring.datasource.username", container::getUsername);
        registry.add("spring.datasource.password", container::getPassword);
    }

    @BeforeAll
    static void setUp() {
        technologyCreateDto = new TechnologyCreateDto("Some new technology");

    }

    @Test
    @WithMockUser(username = USERNAME_ADMIN, authorities = ROLE_ADMIN)
    void create_ValidTechnologyCreateDTO_OK() throws Exception {

        mockMvc.perform(post(URL_PREFIX)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(technologyCreateDto)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.name").value("Some new technology"));
    }

    @Test
    @WithMockUser(username = USERNAME_ADMIN, authorities = ROLE_ADMIN)
    void create_NameAlreadyExists_Conflict() throws Exception {
        Technology technology = new Technology();
        technology.setId(1L);
        technology.setName(technologyCreateDto.name());
        technologyRepository.save(technology);

        mockMvc.perform(post(URL_PREFIX)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(technologyCreateDto)))
            .andExpect(status().isConflict())
            .andExpect(result -> assertTrue(
                result.getResolvedException() instanceof DuplicateException))
            .andExpect(
                result -> assertEquals("Technology with name Some new technology already exists",
                    Objects.requireNonNull(
                        result.getResolvedException()).getMessage()));
    }

    @Test
    @WithMockUser(username = USERNAME_MENTOR, authorities = ROLE_MENTOR)
    void create_ValidTechnologyCreateDTOLoggedWithMentor_Forbidden() throws Exception {

        mockMvc.perform(post(URL_PREFIX)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(technologyCreateDto)))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = USERNAME_ADMIN, authorities = ROLE_ADMIN)
    void create_InvalidTechnologyCreateDTOLoggedWithAdmin_BadRequest() throws Exception {
        TechnologyCreateDto invalidNameTechnologyCreatedTO = new TechnologyCreateDto("");
        mockMvc.perform(post(URL_PREFIX)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(invalidNameTechnologyCreatedTO)))
            .andExpect(status().isBadRequest());
    }

    @Test
    @WithAnonymousUser
    void create_NoLoggedUser_Unauthorized() throws Exception {

        mockMvc.perform(post(URL_PREFIX)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(technologyCreateDto)))
            .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = USERNAME_INTERN, authorities = ROLE_INTERN)
    void create_ValidTechnologyCreateDTOLoggedWithIntern_Forbidden() throws Exception {
        TechnologyCreateDto technologyCreateDto = new TechnologyCreateDto("Some new technology");

        mockMvc.perform(post(URL_PREFIX)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(technologyCreateDto)))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = USERNAME_ADMIN, authorities = ROLE_ADMIN)
    void getById_ValidId_OK() throws Exception {

        mockMvc.perform(get(URL_PREFIX + "/1"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.name").value("Java"));
    }

    @Test
    @WithMockUser(username = USERNAME_ADMIN, authorities = ROLE_ADMIN)
    void getById_InvalidId_throwsNotFound() throws Exception {
        mockMvc.perform(get(URL_PREFIX + "/4"))
            .andExpect(status().isNotFound())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    @WithMockUser(username = USERNAME_INTERN, authorities = ROLE_INTERN)
    void getById_ValidId_ForbiddenForIntern() throws Exception {

        mockMvc.perform(get(URL_PREFIX + "/1"))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = USERNAME_MENTOR, authorities = ROLE_MENTOR)
    void getById_ValidId_ForbiddenForMentor() throws Exception {

        mockMvc.perform(get(URL_PREFIX + "/1"))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithAnonymousUser
    void getById_NoLoggedUser_Unauthorized() throws Exception {

        mockMvc.perform(get(URL_PREFIX + "/1"))
            .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = USERNAME_ADMIN, authorities = ROLE_ADMIN)
    void getPage_LoggedWithAdmin_OK() throws Exception {
        mockMvc.perform(get(URL_PREFIX))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.numberOfElements").value(3));
    }

    @Test
    @WithMockUser(username = USERNAME_MENTOR, authorities = ROLE_MENTOR)
    void getPage_LoggedWithMentor_OK() throws Exception {
        mockMvc.perform(get(URL_PREFIX))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.numberOfElements").value(3));
    }

    @Test
    @WithMockUser(username = USERNAME_INTERN, authorities = ROLE_INTERN)
    void getPage_LoggedWithIntern_OK() throws Exception {
        mockMvc.perform(get(URL_PREFIX))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.numberOfElements").value(3));
    }

    @Test
    @WithAnonymousUser
    void getPage_NoLoggedUser_Unauthorized() throws Exception {

        mockMvc.perform(get(URL_PREFIX))
            .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = USERNAME_ADMIN, authorities = ROLE_ADMIN)
    void update_LoggedWithAdmin_OK() throws Exception {
        TechnologyCreateDto technologyUpdateDTO = new TechnologyCreateDto("C++");
        mockMvc.perform(put(URL_PREFIX + "/3")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(technologyUpdateDTO)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.name").value("C++"));
    }

    @Test
    @WithMockUser(username = USERNAME_MENTOR, authorities = ROLE_MENTOR)
    void update_LoggedWithMentor_Forbidden() throws Exception {
        TechnologyCreateDto technologyUpdateDTO = new TechnologyCreateDto("C++");
        mockMvc.perform(put(URL_PREFIX + "/3")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(technologyUpdateDTO)))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = USERNAME_INTERN, authorities = ROLE_INTERN)
    void update_LoggedWithIntern_Forbidden() throws Exception {
        TechnologyCreateDto technologyUpdateDTO = new TechnologyCreateDto("C++");
        mockMvc.perform(put(URL_PREFIX + "/3")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(technologyUpdateDTO)))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithAnonymousUser
    void update_NoLoggedUser_Unauthorized() throws Exception {
        TechnologyCreateDto technologyUpdateDTO = new TechnologyCreateDto("C++");
        mockMvc.perform(put(URL_PREFIX + "/3")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(technologyUpdateDTO)))
            .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = USERNAME_ADMIN, authorities = ROLE_ADMIN)
    void delete_LoggedWithAdmin_OK() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete(URL_PREFIX + "/3")
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());
    }

    @Test
    @WithMockUser(username = USERNAME_MENTOR, authorities = ROLE_MENTOR)
    void delete_LoggedWithMentor_Forbidden() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete(URL_PREFIX + "/3")
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = USERNAME_INTERN, authorities = ROLE_INTERN)
    void delete_LoggedWithIntern_Forbidden() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete(URL_PREFIX + "/3")
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithAnonymousUser
    void delete_NoLoggedUser_Unauthorized() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete(URL_PREFIX + "/3")
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isUnauthorized());
    }

    static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}