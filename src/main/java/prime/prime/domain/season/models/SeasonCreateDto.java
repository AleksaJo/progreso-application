package prime.prime.domain.season.models;

import java.time.LocalDate;
import java.util.Set;
import javax.validation.constraints.Future;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import prime.prime.domain.season.entity.SeasonDurationType;
import prime.prime.infrastructure.validation.EnumValidation;

public record SeasonCreateDto(
    @NotEmpty(message = "Name is mandatory")
    @Size(min = 2, max = 64, message = "Name must be between 2 and 64 characters long")
    String name,

    @NotNull(message = "Duration value is mandatory")
    @Positive(message = "Duration value must be a positive number")
    Integer durationValue,

    @NotEmpty(message = "Duration type is mandatory")
    @EnumValidation(value = SeasonDurationType.class)
    String durationType,

    @NotNull(message = "Start date is mandatory")
    @FutureOrPresent(message = "Start date cannot be in the past")
    LocalDate startDate,

    @NotNull(message = "End date is mandatory")
    @Future(message = "End date must be in the future")
    LocalDate endDate,

    @NotEmpty(message = "Technologies are mandatory")
    Set<String> technologies,

    Set<Long> mentors,

    Set<Long> interns) {

  public SeasonCreateDto(String name,
      Integer durationValue,
      String durationType,
      LocalDate startDate,
      LocalDate endDate,
      Set<String> technologies
  ) {
    this(name, durationValue, durationType, startDate, endDate, technologies, null, null);
  }
}
