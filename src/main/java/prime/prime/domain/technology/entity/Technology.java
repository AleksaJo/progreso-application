package prime.prime.domain.technology.entity;

import prime.prime.domain.season.entity.Season;
import prime.prime.domain.user.entity.User;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;


@Entity
public class Technology {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false,unique = true)
    @NotBlank(message = "Name is mandatory")
    private String name;

    @ManyToMany(mappedBy = "technologies")
    private List<User> users;

    @ManyToMany(mappedBy = "technologies")
    private List<Season> seasons;

    public Technology(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Technology() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Season> getSeasons() {
        return seasons;
    }

    public void setSeasons(List<Season> seasons) {
        this.seasons = seasons;
    }
}
