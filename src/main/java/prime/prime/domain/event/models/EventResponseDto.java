package prime.prime.domain.event.models;

import java.time.LocalDateTime;

public record EventResponseDto(
        Long id,

        String title,

        String description,

        LocalDateTime startTime,

        Long duration,

        Long creatorId

) {}
