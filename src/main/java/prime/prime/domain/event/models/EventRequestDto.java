package prime.prime.domain.event.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;
import java.util.Set;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import org.hibernate.validator.constraints.Length;

public record EventRequestDto(

    @NotBlank(message = "Title cannot be empty")
    @Length(min = 2, message = "Title must have minimum 2 characters")
    @Length(max = 64, message = "Title can have maximum 64 characters")
    String title,

    @NotNull(message = "Description cannot be null")
    String description,

    @NotNull(message = "Start time cannot be empty")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    LocalDateTime startTime,

    @NotNull(message = "Duration cannot be empty")
    @Positive(message = "Duration cannot be negative or 0!")
    Long duration,

    @NotNull(message = "seasonId cannot be null")
    Long seasonId,

    @NotNull(message = "Optional attendees cannot be null")
    Set<Long> optionalAttendees,

    @NotNull(message = "Required attendees cannot be null")
    Set<Long> requiredAttendees
) {

}
