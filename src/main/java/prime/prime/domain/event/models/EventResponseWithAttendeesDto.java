package prime.prime.domain.event.models;

import java.time.LocalDateTime;
import java.util.Set;

public record EventResponseWithAttendeesDto(
    Long id,

    String title,

    String description,

    LocalDateTime startTime,

    Long duration,

    Long creatorId,

    Long seasonId,

    Set<AttendeesDto> attendees
) {

}
