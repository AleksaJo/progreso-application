package prime.prime.domain.event.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.Positive;
import java.time.LocalDateTime;
import java.util.Set;

public record EventUpdateDto(
        @Length(min = 2, message = "Title must have minimum 2 characters")
        @Length(max = 64, message = "Title can have maximum 64 characters")
        String title,

        String description,

        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
        LocalDateTime startTime,

        @Positive(message = "Duration cannot be negative or 0!")
        Long duration,

        boolean notifyAttendees,

        Set<Long> optionalAttendees,

        Set<Long> requiredAttendees
) {
}
