package prime.prime.domain.user.repository;

import org.springframework.data.jpa.domain.Specification;
import prime.prime.domain.account.entity.AccountStatus;
import prime.prime.domain.role.Role;
import prime.prime.domain.user.entity.User;
import prime.prime.domain.user.models.SearchUserDto;

import javax.persistence.criteria.*;
import java.lang.reflect.Field;

public class UserSpecification implements Specification<User> {

    private final SearchUserDto searchUserDto;

    public UserSpecification(SearchUserDto searchUserDto) {
        this.searchUserDto = searchUserDto;
    }

    @Override
    public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query,
        CriteriaBuilder criteriaBuilder) {
        Predicate p = criteriaBuilder.conjunction();

        Field[] declaredFields = searchUserDto.getClass().getDeclaredFields();

        for (Field searchField : declaredFields) {
            searchField.setAccessible(true);

            String searchValue;
            try {
                searchValue = (String) searchField.get(searchUserDto);
                if (searchValue == null) {
                    continue;
                }
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }

            Predicate predicate;
            if (searchField.getName().equals("role")) {
                predicate = criteriaBuilder.equal(
                    root.get("account").get(searchField.getName()), Role.valueOf(searchValue));
            } else if (searchField.getName().equals("status")) {
                predicate = criteriaBuilder.equal(
                    root.get("account").get(searchField.getName()),
                    AccountStatus.valueOf(searchValue));
            } else {
                predicate = criteriaBuilder.like(
                    root.get(searchField.getName()), '%' + searchValue + '%');
            }

            p.getExpressions().add(criteriaBuilder.and(predicate));
        }

        return p;
    }
}
