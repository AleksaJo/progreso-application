package prime.prime.domain.eventrequest.models;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

public record EventRequestCreateDto(
        @NotBlank(message = "Title cannot be empty")
        @Length(min = 2, max = 64, message = "Title must be between 2 and 64 symbols")
        String title,

        String description,

        @NotNull(message = "Season cannot be null")
        Long seasonId
) {
}
