package prime.prime.domain.eventrequest.models;

import org.hibernate.validator.constraints.Length;

public record EventRequestUpdateDto(
        @Length(min = 2, max = 64, message = "Title must be between 2 and 64 symbols")
        String title,

        String description
) {
}
