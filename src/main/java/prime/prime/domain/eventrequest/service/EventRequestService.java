package prime.prime.domain.eventrequest.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import prime.prime.domain.eventrequest.models.EventRequestCreateDto;
import prime.prime.domain.eventrequest.models.EventRequestReturnDto;
import prime.prime.domain.eventrequest.models.EventRequestStatusDto;
import prime.prime.domain.eventrequest.models.EventRequestUpdateDto;
import prime.prime.domain.eventrequest.models.SearchEventRequestDto;
import prime.prime.infrastructure.security.ProgresoUserDetails;

public interface EventRequestService {

    EventRequestReturnDto create(EventRequestCreateDto eventRequestDto, ProgresoUserDetails user);

    Page<EventRequestReturnDto> getAll(SearchEventRequestDto searchEventRequestDto,
        Pageable pageable, ProgresoUserDetails userDetails);

    EventRequestReturnDto getById(Long id);

    EventRequestReturnDto update(Long id, EventRequestUpdateDto eventRequestUpdateDto);

    EventRequestReturnDto changeStatus(Long id, EventRequestStatusDto eventRequestStatusDTO);

    void deleteEventRequest(Long id);
}
