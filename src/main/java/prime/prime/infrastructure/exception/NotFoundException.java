package prime.prime.infrastructure.exception;

public class NotFoundException extends RuntimeException {

    public NotFoundException(String className, String fieldName, String fieldValue) {
        super(className + " with " + fieldName + " : " + fieldValue + " not found;");
    }

}
