package prime.prime.infrastructure.exception;

public class EventRequestException extends RuntimeException {

    public EventRequestException() {
        super("The request has already been rejected");
    }

    public EventRequestException(String message) {
        super(message);
    }
}
