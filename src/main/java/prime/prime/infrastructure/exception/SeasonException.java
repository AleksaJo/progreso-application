package prime.prime.infrastructure.exception;

import prime.prime.domain.account.entity.AccountStatus;
import prime.prime.domain.role.Role;

public class SeasonException extends RuntimeException {

    public SeasonException(String className, String fieldName, String fieldValue, int limit) {
        super(className + " with " + fieldName + " : " + fieldValue
            + " have already been assigned to "
            + limit + " seasons!");
    }

    public SeasonException(String className, String fieldName, String fieldValue, Role role) {
        super(className + " with " + fieldName + " : " + fieldValue + " doesn't have the role "
            + role.name());
    }

    public SeasonException(String className, String fieldName, String fieldValue) {
        super("Account of " + className + " with " + fieldName + " : " + fieldValue + " is not "
            + AccountStatus.ACTIVE.name());
    }

    public SeasonException(Long fieldValue, Long seasonId) {
        super("User with id " + fieldValue + " is not assigned to season with id " + seasonId);
    }

    public SeasonException(String message) {
        super(message);
    }
}