package prime.prime.infrastructure.exception;

public class DuplicateException extends RuntimeException {
    private String message;

    public DuplicateException(String className, String fieldName, String fieldValue) {
        super(className + " with " + fieldName + " " + fieldValue + " already exists");
    }

    public DuplicateException(String message) {
        this.message = message;
    }
}
