package prime.prime.infrastructure.exception;

public class MultipleAttendeesException extends RuntimeException {
    public MultipleAttendeesException(String message) {
        super(message);
    }
}
