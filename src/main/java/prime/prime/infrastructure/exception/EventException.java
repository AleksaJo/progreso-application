package prime.prime.infrastructure.exception;

public class EventException extends RuntimeException {


    public EventException(String message) {
        super(message);
    }

}
